package com.wen.health.controller;

import com.wen.health.entity.Result;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

//  业务层一旦出现异常  该对象就会捕捉到
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler
    public Result exceptionHandle(Exception ex){
        ex.printStackTrace();
        String errorMsg = ex.getMessage(); //  异常信息
        if(errorMsg.length() > 200)
            return new Result(false,errorMsg.substring(0,200) + "...");
        else
            return new Result(false,errorMsg);
    }

}
