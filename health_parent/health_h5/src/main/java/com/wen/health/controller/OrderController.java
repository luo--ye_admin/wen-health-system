package com.wen.health.controller;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.wen.health.entity.Result;
import com.wen.health.service.OrderService;
import com.wen.health.utils.pay.PayUtils;
import com.wen.health.utils.redis.RedisUtil;
import com.wen.health.utils.resources.RedisConstant;
import com.wen.health.utils.resources.RedisMessageConstant;
import com.wen.health.utils.sms.ValidateCodeUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @author 十一
 * @Description
 * @create 2020-11-10-19:48
 */
@RestController
@RequestMapping("order")
@Api(tags = "移动端-预约管理")
public class OrderController {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Reference
    private OrderService orderService;

    //发送预约验证码
    @GetMapping("sendCode/{telephone}")
    @ApiOperation(value = "发送手机验证码",notes = "移动端验证码发送")
    public Result findAllSetMeal(@PathVariable("telephone") String telephone){
        //生成手机验证码
        String code = ValidateCodeUtils.generateValidateCode(4);
        //添加进Redis缓存中
        RedisUtil.set(RedisMessageConstant.SENDTYPE_ORDER+telephone,code,90, TimeUnit.SECONDS);
        //将手机号和验证码天剑进消息队列】
        Map<String,String> map = new HashMap();
        map.put("telephone",telephone);
        map.put("code",code);
        //todo：
        rabbitTemplate.convertAndSend("simple-queue",map);
        return new Result(code);
    }

    //查询所有的预约套餐
    @PostMapping("add")
    @ApiOperation(value = "预约",notes = "提交用户预约数据")
    public Result add(@RequestBody Map map){
       return new Result(orderService.add(map));
    }

    //查询订单详细信息
    @GetMapping("findOrderInfoByOrderId/{id}")
    @ApiOperation(value = "查询订单详细信息",notes = "根据订单id查询订单详情")
    public Result findOrderInfoByOrderId(@PathVariable("id") int id){
        return new Result(orderService.findOrderInfoByOrderId(id));
    }
    /**
     * 通过预约订单id生成url
     *
     * @param oid 预约订单编号
     * @return String的支付URL
     * @author liusen
     */
    @GetMapping("wxPayByOrderId/{oid}")
    public Result wxPayByOderId(@PathVariable("oid") String oid) {
        // 查询数据库此订单号是否存在，避免地址栏直接访问
        if (!orderService.isExistOrderId(oid)) {
            throw new RuntimeException("订单编号不存在，请重新预约！");
        }

        // 生成微信支付URL连接
        String pay_url = PayUtils.createOrder(oid, 1);
        return new Result(pay_url);
    }

    /**
     * 用户成功支付后执行的方法
     *
     * @param httpServletRequest  微信支付成功请求
     * @param httpServletResponse 响应微信的成功请求
     * @author liusen
     */
    @PostMapping("payNotify")
    public void payNotify(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
        try {
            // 1.接收请求参数（xml）
            ServletInputStream in = httpServletRequest.getInputStream();
            // 2.将xml转为java对象
            XmlMapper xmlMapper = new XmlMapper();
            Map param = xmlMapper.readValue(in, Map.class);
            String oid = (String) param.get("out_trade_no");
            System.out.println("oid ======== " + oid);
            // 3.修改订单状态已支付
            orderService.updateStatus(oid);
            // 移除redis - set中的元素
            RedisUtil.removeSetMember(RedisConstant.SET_ORDER_ID, oid);


            // 4. 发送手机短信
            String phoneNumber = orderService.findPhonenumberByOid(oid);
            Map<String, String> map = new HashMap();
            map.put("telephone", phoneNumber);
            map.put("orderId", oid.substring(oid.length() - 5));
            //todo:
            rabbitTemplate.convertAndSend("payNotify-queue",map);


            // 5.返回微信平台，接收成功..
            HashMap<String, String> result = new HashMap<>();
            result.put("return_code", "SUCCESS");
            result.put("return_msg", "OK");
            // 将map转为xml
            String xml = xmlMapper.writeValueAsString(result);
            httpServletResponse.setContentType("application/xml;charset=utf-8");
            httpServletResponse.getWriter().write(xml);
            System.out.println("===========已经响应微信的支付成功通知！=================");
        } catch (Exception e) {
            throw new RuntimeException("支付响应失败！");
        }
    }


    /**
     * 查询订单支付状态
     * @param oid 订单编号
     * @return 订单状态：1-已支付，0-未支付
     * @author liusen
     */
    @GetMapping("findPayStatus/{oid}")
    public Result findPayStatus(@PathVariable("oid") String oid) {
        return new Result(orderService.findPayStatus(oid));
    }

    /**
     * 删除订单信息
     * @param oid 订单id
     * @author liusen
     */
    @DeleteMapping("deleteOrderByOrderId/{oid}")
    public Result deleteOrderByOrderId(@PathVariable("oid") String oid) {
        orderService.deleteOrderByOrderId(oid);
        return new Result(true,"删除成功");
    }

}
