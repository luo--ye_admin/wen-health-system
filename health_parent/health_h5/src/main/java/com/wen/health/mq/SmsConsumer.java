package com.wen.health.mq;

import com.wen.health.utils.sms.SmsUtils;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
import java.util.Map;

@Component
public class SmsConsumer {

    @RabbitListener(queues = "simple-queue")
    public  void  send(Map<String,String> map){
        String telephone = map.get("telephone");
        String code = map.get("code");
        SmsUtils.validUserTelephone(telephone,code);
    }

    @RabbitListener(queues = "payNotify-queue")
    public  void  notify(Map<String,String> map){
        String telephone = map.get("telephone");
        String orderId = map.get("orderId");
        SmsUtils.orderSuccessMessage(telephone,orderId);
    }
}
