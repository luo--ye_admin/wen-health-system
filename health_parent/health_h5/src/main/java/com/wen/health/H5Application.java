package com.wen.health;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author 十一
 * @Description
 * @create 2020-11-10-17:23
 */
@SpringBootApplication
public class H5Application {
    public static void main(String[] args) {
        SpringApplication.run(H5Application.class,args);
        System.out.println("H5端启动成功！");
    }
}
