package com.wen.health.controller;

import com.wen.health.entity.Result;
import com.wen.health.service.SetMealService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 十一
 * @Description
 * @create 2020-11-10-19:48
 */
@RestController
@RequestMapping("setMeal")
@Api(tags = "移动端-套餐管理")
public class SetMealController {

    @Reference
    private SetMealService setMealService;

    //查询所有的预约套餐
    @GetMapping("findAllSetMeal")
    @ApiOperation(value = "查询所有套餐列表",notes = "移动端套餐列表查询")
    public Result findAllSetMeal(){
        return new Result(setMealService.findAllSetMeal());
    }

    //预约套餐详细
    @GetMapping("findSetMealDetail/{id}")
    @ApiOperation(value = "套餐详情",notes = "移动端套餐列表查询")
    public Result findSetMealDetail(@PathVariable("id") int id){
        return new Result(setMealService.findSetMealDetail(id));
    }

    //查询单个套参信息
    @GetMapping("findSetMealById/{id}")
    @ApiOperation(value = "查询单个套餐详情", notes = "移动端套餐列表查询")
    public Result findSetMealById(@PathVariable("id") int id){
        return new Result(setMealService.findSetMealById(id));
    }


}
