package com.wen.health.utils.jwt;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@ToString
public class UserInfo {

    private Long id;

    private String username;

    private String password;

     private String roles; //将用户权限转化为字符串形式才能进行加密

    public UserInfo(Long id, String username, String roles) {
        this.id = id;
        this.username = username;
        this.roles = roles;
    }
}
