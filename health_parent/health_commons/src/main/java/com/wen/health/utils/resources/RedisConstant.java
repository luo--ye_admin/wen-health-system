package com.wen.health.utils.resources;

public class RedisConstant {
    //套餐图片保存在阿里云的图片名称
    public static final String ALL_SETMEAL_PIC_SET = "allsetmealPicSet";
    public static final String SINGLE_PIC = "single:file:";
    public static final String SET_ORDER_ID = "SET_ORDER_ID";
    public static final String ORDER_OID = "order:oid:";
}
