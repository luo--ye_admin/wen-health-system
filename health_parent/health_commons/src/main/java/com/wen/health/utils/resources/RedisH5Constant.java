package com.wen.health.utils.resources;

public class RedisH5Constant {
    //H5页面套餐列表和套餐详情缓存
    public static final String ALL_SETMEAL = "allsetmeal";
    public static final String SETMEAL_DETAIL = "setmealdetail:id:";

}
