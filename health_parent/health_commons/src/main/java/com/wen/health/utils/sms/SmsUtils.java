package com.wen.health.utils.sms;

import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;

/**
 * 短信发送工具类
 */
public class SmsUtils {
    public static final String VALIDATE_CODE = "SMS_205434127";//发送短信验证码
    public static final String ORDER_NOTICE = "SMS_205469471";//体检预约成功通知
    public static final String ORDER_NOTICE2 = "SMS_205443793";//发送短信验证码(刘森)

    /**
     * 发送短信验证码
     * @param phoneNumbers
     * @param param

     * @throws ClientException
     */
    public static void validUserTelephone(String phoneNumbers,String param)  {
        DefaultProfile profile = DefaultProfile.getProfile("cn-shanghai", "LTAI4GHPXLus5HsRhpm131bQ", "6ucwUyf0gJpZQKJsWNvHHUaO0xcnam");
        IAcsClient client = new DefaultAcsClient(profile);

        CommonRequest request = new CommonRequest();
        request.setSysMethod(MethodType.POST);
        request.setSysDomain("dysmsapi.aliyuncs.com");
        request.setSysVersion("2017-05-25");
        request.setSysAction("SendSms");
        request.putQueryParameter("RegionId", "cn-hangzhou");
        request.putQueryParameter("PhoneNumbers", phoneNumbers);
        request.putQueryParameter("SignName", "肥宅有");
        request.putQueryParameter("TemplateCode", ORDER_NOTICE2);
        request.putQueryParameter("TemplateParam", "{\"code\":\""+param+"\"}");
        try {
            CommonResponse response = client.getCommonResponse(request);
            System.out.println(response.getData());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void orderSuccessMessage(String phoneNumbers,String param)  {
        DefaultProfile profile = DefaultProfile.getProfile("cn-shanghai", "LTAI4G6Dcc5bhfLu4YwJsiUK", "g7j91KoRbIdFCOjk08CiWvP5xWjrOz");
        IAcsClient client = new DefaultAcsClient(profile);

        CommonRequest request = new CommonRequest();
        request.setSysMethod(MethodType.POST);
        request.setSysDomain("dysmsapi.aliyuncs.com");
        request.setSysVersion("2017-05-25");
        request.setSysAction("SendSms");
        request.putQueryParameter("RegionId", "cn-hangzhou");
        request.putQueryParameter("PhoneNumbers", phoneNumbers);
        request.putQueryParameter("SignName", "白菜健康");
        request.putQueryParameter("TemplateCode", ORDER_NOTICE);
        request.putQueryParameter("TemplateParam", "{\"code\":\""+param+"\"}");
        try {
            CommonResponse response = client.getCommonResponse(request);
            System.out.println(response.getData());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //1234
    public static void main(String[] args) throws ClientException {
        // SMSUtils.sendShortMessage("SMS_169641911","15688268846","2345");
        //SmsUtils.orderSuccessMessage("15556602698","6666");
    }
}
