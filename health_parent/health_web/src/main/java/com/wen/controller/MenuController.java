package com.wen.controller;


import com.wen.health.entity.QueryPageBean;
import com.wen.health.entity.Result;
import com.wen.health.pojo.Menu;
import com.wen.health.service.MenuService;
import com.wen.health.utils.redis.RedisUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Api(tags = "菜单管理开发")
public class MenuController {
    @Reference
    private MenuService menuService;

    //菜单的动态显示
    @GetMapping("menu/findMenuByUsername/{username}")
    public Result findMenuByUsername(@PathVariable("username") String username){
        return  new Result(menuService.findMenuByUsername(username));
    }

    //菜单的分页查询
    @PostMapping("menu/findPage")
    @ApiOperation(value = "分页查询",notes = "根据客户端请求，完成分页查询")//   添加业务方法上
//    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @ApiImplicitParams({
            @ApiImplicitParam(name="currentPage",value="当前分页页码",required=true,paramType="form",dataType="Integer"),
            @ApiImplicitParam(name="pageSize",value="每页显示记录数",required=true,paramType="form",dataType="Integer"),
            @ApiImplicitParam(name="queryString",value="查询条件",required=false,paramType="form",dataType="String")
    })
    public Result findPage(@RequestBody QueryPageBean queryPageBean){
        System.out.println("queryPageBean = " + queryPageBean);
        return new Result(menuService.findPage(queryPageBean));
    }

    //菜单的添加
    //菜单的修改
    @PostMapping("menu/add")
    public Result add(@RequestBody Menu menu){
        boolean b = menuService.saveOrUpdate(menu);
        RedisUtil.removeKeys("findAllMenu");
        return new Result(b);
    }
    //菜单的删除
    @DeleteMapping("menu/deleteMenuById/{id}")
    public Result deleteMenuById(@PathVariable("id") int id){
        Boolean b = menuService.deleteMenuByMId(id);
        RedisUtil.removeKeys("findAllMenu");
        return  new Result(b);
    }

    //添加角色的时候要查询所有的菜单，用于添加角色和菜单中间表
    @GetMapping("menu/findAll")
    public Result findAll(){
        List findAll = RedisUtil.get("findAllMenu");
        if(findAll!=null){
            return new Result(findAll);
        }else{
            List<Menu> list = menuService.list();
            RedisUtil.set("findAllMenu",list);
            return  new Result(list);
        }
    }
}
