package com.wen.controller;

import com.wen.health.dto.RoleDTO;
import com.wen.health.entity.QueryPageBean;
import com.wen.health.entity.Result;
import com.wen.health.service.RoleService;
import io.swagger.annotations.Api;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.*;

@RestController
@Api(tags = "角色管理开发")
public class RoleController {

    @Reference
    private RoleService roleService;

    @GetMapping("role/findAll")
    public Result findAll(){
        return   new Result(roleService.list()); //   total  +  List
    }


    @PostMapping("role/findPage")
    public Result findPage(@RequestBody QueryPageBean queryPageBean){
        return   new Result(roleService.findPage(queryPageBean)); //   total  +  List
    }

    @PostMapping("role/add")
    public Result add(@RequestBody RoleDTO roleDTO) {
        return new Result(roleService.add(roleDTO));
    }

    @GetMapping("role/findPermissionInfoByRoleId/{id}")
    public Result findPermissionInfoByRoleId(@PathVariable("id") Integer id){
        return new Result(roleService.findPermissionInfoByRoleId(id));
    }
    @GetMapping("role/findMenuInfoByRoleId/{id}")
    public Result findMenuInfoByRoleId(@PathVariable("id") Integer id){
        return new Result(roleService.findMenuInfoByRoleId(id));
    }

    @DeleteMapping("role/deleteRoleById/{id}")
    public Result deleteRoleById(@PathVariable("id") int id){
        return new Result(roleService.deleteRoleById(id));
    }
}
