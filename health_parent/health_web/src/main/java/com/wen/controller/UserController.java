package com.wen.controller;

import com.wen.health.entity.QueryPageBean;
import com.wen.health.entity.Result;
import com.wen.health.pojo.User;
import com.wen.health.service.UserService;
import com.wen.health.utils.redis.RedisUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.*;

@RestController
@Api(tags = "套餐管理开发")
public class UserController {

    @Reference
    private UserService userService;

    //权限分页查询
    @PostMapping("user/findPage")
    @ApiOperation(value = "分页查询",notes = "根据客户端请求，完成分页查询")//   添加业务方法上
//    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @ApiImplicitParams({
            @ApiImplicitParam(name="currentPage",value="当前分页页码",required=true,paramType="form",dataType="Integer"),
            @ApiImplicitParam(name="pageSize",value="每页显示记录数",required=true,paramType="form",dataType="Integer"),
            @ApiImplicitParam(name="queryString",value="查询条件",required=false,paramType="form",dataType="String")
    })
    public Result findPage(@RequestBody QueryPageBean queryPageBean){
        return new Result(userService.findPage(queryPageBean));
    }

    // 用戶的添加和修改
    @PostMapping("user/add")
    public Result add(@RequestBody User user){
        boolean b = userService.saveOrUpdate(user);
        RedisUtil.removeKeys("findAllUser");
        return new Result(b);
    }

    //用戶的删除
    @DeleteMapping("user/deleteCheckItemById/{id}")
    public Result deleteCheckItemById(@PathVariable("id") int id){
        boolean b = userService.removeById(id);
        RedisUtil.removeKeys("findAllUser");
        return  new Result(b);
    }

    //添加用戶的時候查詢所有用戶，用來校驗用戶名是否重复
    @GetMapping("user/findAll/{username}")
    public Result findAll(@PathVariable("username") String username){
        Integer userByUsername = userService.findUserByUsername(username);
        if(userByUsername > 0){
            return  new Result(true);
        }else{
            return new Result(false);
        }
    }



}
