package com.wen.controller;

import com.wen.health.entity.Result;
import com.wen.health.service.OrderSettingService;
import com.wen.health.utils.poi.POIUtils;
import io.swagger.annotations.Api;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@Api(tags = "预约管理开发")  // 作用在类上
@RequestMapping("orderSetting")
public class OrderSettingController {
    @Reference
    private OrderSettingService orderSettingService;

    //文件上传
    @PostMapping("importOrderSettings")
    public Result importOrderSettings(@RequestParam("excelFile")MultipartFile file){
        try {
            //获取文件内容
            List<String[]> orderSettingList = POIUtils.readExcel(file);
            //解析文件并上传数据库
            return new Result(orderSettingService.importOrderSettings(orderSettingList));
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    //查询预约数据
    @GetMapping("findSettingData/{year}/{month}")
    public Result findSettingData(@PathVariable("year") int year,@PathVariable("month") int month){
        return new Result(orderSettingService.findSettingData(year,month));
    }

    //修改最大预约数
    @PutMapping("updateNumberByOrderDate/{number}/{orderDate}")
    public Result updateNumberByOrderDate(@PathVariable("number") int number,@PathVariable("orderDate") String orderDate){
        return  new Result(orderSettingService.updateNumberByOrderDate(number,orderDate));
    }

}








