package com.wen.controller;

import com.wen.health.dto.SetmealDTO;
import com.wen.health.entity.QueryPageBean;
import com.wen.health.entity.Result;
import com.wen.health.service.SetMealService;
import com.wen.health.utils.aliyunoss.AliyunUtils;
import com.wen.health.utils.redis.RedisUtil;
import com.wen.health.utils.resources.RedisConstant;
import com.wen.health.utils.resources.UploadUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("setMeal")
@Api(tags = "套餐管理开发")  // 作用在类上
public class SetMealController {

    //从dubbo上获取服务节点
    @Reference
    private SetMealService setMealService;

    @PostMapping("findPage")
    @ApiOperation(value = "分页查询",notes = "根据客户端请求，完成分页查询")//   添加业务方法上
    @ApiImplicitParams({
            @ApiImplicitParam(name="currentPage",value="当前分页页码",required=true,paramType="form",dataType="Integer"),
            @ApiImplicitParam(name="pageSize",value="每页显示记录数",required=true,paramType="form",dataType="Integer"),
            @ApiImplicitParam(name="queryString",value="查询条件",required=false,paramType="form",dataType="String")
    })
    public Result findPage(@RequestBody QueryPageBean queryPageBean) {
        return new Result(setMealService.findPage(queryPageBean));
    }

    //文件上传
    @PostMapping("upload")
    public Result upload(@RequestParam("imgFile") MultipartFile file){
        try {
            //获取源文件名
            String originalFilename = file.getOriginalFilename();
            String uuidFileName = UploadUtils.generateRandonFileName(originalFilename);
            //1、上传进阿里云OSS服务器
            AliyunUtils.uploadMultiPartFileToAliyun(file.getBytes(),uuidFileName);
            //2、添加进Redis的set集合里
            RedisUtil.addToSet(RedisConstant.ALL_SETMEAL_PIC_SET,uuidFileName);
            //3、添加进key-value中，用于删除垃圾文件时判断是否超时
            RedisUtil.set(RedisConstant.SINGLE_PIC+uuidFileName,uuidFileName,5, TimeUnit.MINUTES);
            return new Result(uuidFileName);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
    }

    //添加或修改预约套餐
    @PostMapping("addOrUpdate")
    public Result addOrUpdate(@RequestBody SetmealDTO setmealDTO){
        return new Result(setMealService.addOrUpdate(setmealDTO));
    }

    //查询套餐的检查组信息
    @GetMapping("findCheckGroupsBySetMealId/{id}")
    @ApiOperation(value = "查询套餐检查组详情", notes = "移动端套餐列表查询")
    public Result findCheckGroupsBySetMealId(@PathVariable("id") int id){
        return new Result(setMealService.findCheckGroupsBySetMealId(id));
    }

    //逻辑删除套餐信息
    @DeleteMapping("delete/{id}")
    public Result deleteSetMeal(@PathVariable("id") int id){
        return new Result(setMealService.deleteSetMeal(id));
    }
}








