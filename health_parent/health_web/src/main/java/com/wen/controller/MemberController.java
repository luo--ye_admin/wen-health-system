package com.wen.controller;

import com.wen.health.entity.Result;
import com.wen.health.service.MemberService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("report")
@Api(tags = "会员管理开发")  // 作用在类上
public class MemberController {

    //从dubbo上获取服务节点
    @Reference
    private MemberService memberService;

    @GetMapping("findMemberProportionByAge")
    @ApiOperation(value = "会员占比查询",notes = "根据会员的生日，查询不同年龄阶段的会员数量占比")//   添加业务方法上
    public Result findMemberProportionByAge() {
        List<Map> mapList =memberService.findMemberProportionByAge();
        return new Result(mapList);
    }


    //会员占比（按照性别男女比例进行查询）
    @GetMapping("findMemberProportionBySex")
    @ApiOperation(value = "会员占比查询",notes = "根据会员的生日，查询不同年龄阶段的会员数量占比")//   添加业务方法上
    public Result findMemberProportionBySex() {
        List<Map> mapList =memberService.findMemberProportionBySex();
        return new Result(mapList);
    }

}








