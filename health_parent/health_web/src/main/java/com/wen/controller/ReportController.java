package com.wen.controller;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.TemplateExportParams;
import com.wen.health.dto.ReportDto;
import com.wen.health.entity.Result;
import com.wen.health.service.ReportService;
import com.wen.health.utils.date.DateUtils;
import com.wen.health.vo.ReportVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.dubbo.config.annotation.Reference;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.Date;
import java.util.Map;

@RestController
@RequestMapping("report")
@Api(tags = "套餐管理开发")  // 作用在类上
public class ReportController {
    @Reference
    private ReportService reportService;

    /**
     * 会员占比
     * @return
     */
    @GetMapping("setmealProportion")
    public Result setmealProportion(){
        return new Result(reportService.setmealProportion());
    }

    /**
     * 会员数量统计
     * @return
     */
    @PostMapping("memberReportByLastYear")
    public Result memberReportByLastYear(@RequestBody ReportDto reportDto ){
        ReportVo reportResult = reportService.memberReportByLastYear(reportDto);
        return   new Result(reportResult);
    }

    @ApiOperation(value = "运营数据查询", notes = "查询运营报表所需数据")
    @GetMapping("findBusinessReportData")
    public Result findBusinessReportData() {
        return new Result(reportService.findBusinessReportData());
    }

    @ApiOperation(value = "运营报表excel导出", notes = "导出当如运营报表excel表格")
    @GetMapping("downloadBusinessReport")
    public Result downloadBusinessReport(HttpServletResponse response) {
        Map businessReportData = reportService.findBusinessReportData();
        TemplateExportParams params = new TemplateExportParams("static/template/BusinessReportTemplate.xlsx");
        Workbook wb = ExcelExportUtil.exportExcel(params,businessReportData);
        OutputStream out = null;
        try {
            String today = DateUtils.parseDate2String(new Date(), "yyyy-MM-dd");
            String fileName = today + "运营报表.xlsx";// 文件名
            response.setContentType("application/x-msdownload");
            response.setHeader("Content-Disposition", "attachment; filename="
                    + URLEncoder.encode(fileName, "UTF-8"));
            out = response.getOutputStream();
            wb.write(out);
            wb.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

}
