package com.wen.controller;


import com.wen.health.entity.QueryPageBean;
import com.wen.health.entity.Result;
import com.wen.health.pojo.Permission;
import com.wen.health.service.PermissionService;
import com.wen.health.utils.redis.RedisUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Api(tags = "权限管理开发")
public class PermissionController {
    @Reference
   private PermissionService permissionService;
    //权限分页查询
    @PostMapping("permission/findPage")
    @ApiOperation(value = "分页查询",notes = "根据客户端请求，完成分页查询")//   添加业务方法上
//    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @ApiImplicitParams({
            @ApiImplicitParam(name="currentPage",value="当前分页页码",required=true,paramType="form",dataType="Integer"),
            @ApiImplicitParam(name="pageSize",value="每页显示记录数",required=true,paramType="form",dataType="Integer"),
            @ApiImplicitParam(name="queryString",value="查询条件",required=false,paramType="form",dataType="String")
    })
    public Result findPage(@RequestBody QueryPageBean queryPageBean){
        return new Result(permissionService.findPage(queryPageBean));
    }
// 权限的添加和修改
@PostMapping("permission/add")
public Result add(@RequestBody Permission permission){
    boolean b = permissionService.saveOrUpdate(permission);
    RedisUtil.removeKeys("findAllPermission");
    return new Result(b);
}
//权限的删除
@DeleteMapping("permission/deletePermissionById/{id}")
public Result deleteCheckItemById(@PathVariable("id") int id){
    Boolean b = permissionService.deletePermissionAndRoleByPid(id);
    RedisUtil.removeKeys("findAllPermission");
    return  new Result(b);
}

//添加角色的时候查询所有的权限

    @GetMapping("permission/findAll")
    public Result findAll(){
        List findAll = RedisUtil.get("findAllPermission");
        if(findAll!=null){
           return new Result(findAll);
        }else{
            List<Permission> list = permissionService.list();
            RedisUtil.set("findAllPermission",list);
            return  new Result(list);
        }
    }
}
