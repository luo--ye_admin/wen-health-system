package com.wen.controller;

import com.wen.health.entity.QueryPageBean;
import com.wen.health.entity.Result;
import com.wen.health.pojo.CheckItem;
import com.wen.health.service.CheckItemService;
import com.wen.health.utils.redis.RedisUtil;
import com.wen.health.utils.resources.RedisH5Constant;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Api(tags = "检查项管理开发")  // 作用在类上
public class CheckItemController {

    //从dubbo上获取服务节点
    @Reference
    private CheckItemService checkItemService;

    //  查询所有的检查项信息   Result   响应数据规范   json 格式数据
    //   后续业务出错  bug   采用全局异常捕捉  响应前端
    @ApiOperation(value = "检查项列表查询",notes = "根据客户端请求，完成检查项所有信息列表查询")//   添加业务方法上
    @GetMapping("checkitem/findAll")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public Result findAll(){
        return   new Result(checkItemService.list());
    }

    //分页查询
    @PostMapping("checkitem/findPage")
    @ApiOperation(value = "分页查询",notes = "根据客户端请求，完成分页查询")//   添加业务方法上
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @ApiImplicitParams({
            @ApiImplicitParam(name="currentPage",value="当前分页页码",required=true,paramType="form",dataType="Integer"),
            @ApiImplicitParam(name="pageSize",value="每页显示记录数",required=true,paramType="form",dataType="Integer"),
            @ApiImplicitParam(name="queryString",value="查询条件",required=false,paramType="form",dataType="String")
    })
    public Result findPage(@RequestBody QueryPageBean queryPageBean){
        return   new Result(checkItemService.findPage(queryPageBean)); //   total  +  List
    }

    //当前端传递主键id时，执行修改SQL，当前端未传递id时，实行添加SQL
    @PostMapping("checkitem/add")
    public Result add(@RequestBody CheckItem checkItem){
        //检查项信息修改 删除套餐详情缓存数据
        RedisUtil.removeKeys(RedisH5Constant.SETMEAL_DETAIL);
        return new Result(checkItemService.saveOrUpdate(checkItem));
    }

    //执行逻辑删除
    @DeleteMapping("checkitem/deleteCheckItemById/{id}")
    public Result deleteCheckItemById(@PathVariable("id") int id){
        //检查项信息修改 删除套餐详情缓存数据
        RedisUtil.removeKeys(RedisH5Constant.SETMEAL_DETAIL);
        return  new Result(checkItemService.deleteById(id));
    }

    //恢复数据
    @GetMapping("checkitem/recovery/{id}")
    public Result recovery(@PathVariable("id") int id){
    return new Result(checkItemService.recovery(id));
    }

    //查询被删除的数据
    @PostMapping("checkitem/findDeletedPage")
    public Result findDeletedPage(@RequestBody QueryPageBean queryPageBean){
        return   new Result(checkItemService.findDeletedPage(queryPageBean)); //   total  +  List
    }

    //恢复选中数据
    @PostMapping("checkitem/restoreAll")
    public Result restoreAll(@RequestBody List<CheckItem> list){
     return new Result(checkItemService.restoreAll(list));
    }
}








