package com.wen.controller;

import com.wen.health.dto.CheckGroupDTO;
import com.wen.health.entity.QueryPageBean;
import com.wen.health.entity.Result;
import com.wen.health.service.CheckGroupService;
import com.wen.health.utils.redis.RedisUtil;
import com.wen.health.utils.resources.RedisH5Constant;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.*;

@RestController
@Api(tags = "检查组管理开发")  // 作用在类上
public class CheckGroupController {

    //从dubbo上获取服务节点
    @Reference
    private CheckGroupService checkGroupService;

    //@ApiOperation(value = "检查项列表查询",notes = "根据客户端请求，完成检查项所有信息列表查询")//   添加业务方法上
    @GetMapping("checkGroup/findAllCheckItemByGroupId/{id}")
    public Result findAllCheckItemByGroupId(@PathVariable("id") int id){
        return new Result(checkGroupService.findAllCheckItemByGroupId(id));
    }

    @GetMapping("checkGroup/findAll")
    public Result findAll(){
        return new Result(checkGroupService.list());
    }

    //分页查询
    @PostMapping("checkGroup/findPage")
    @ApiOperation(value = "分页查询",notes = "根据客户端请求，完成分页查询")//   添加业务方法上
    @ApiImplicitParams({
            @ApiImplicitParam(name="currentPage",value="当前分页页码",required=true,paramType="form",dataType="Integer"),
            @ApiImplicitParam(name="pageSize",value="每页显示记录数",required=true,paramType="form",dataType="Integer"),
            @ApiImplicitParam(name="queryString",value="查询条件",required=false,paramType="form",dataType="String")
    })
    public Result findPage(@RequestBody QueryPageBean queryPageBean){
        return new Result(checkGroupService.findPage(queryPageBean)); //   total  +  List
    }

    //当前端传递主键id时，执行修改SQL，当前端未传递id时，实行添加SQL
    @PostMapping("checkGroup/add")
    public Result addOrUpdate(@RequestBody CheckGroupDTO checkGroupDTO){
        //检查组信息修改 删除套餐详情缓存数据
        RedisUtil.removeKeys(RedisH5Constant.SETMEAL_DETAIL);
        return new Result(checkGroupService.addOrUpdate(checkGroupDTO));
    }

    //执行逻辑删除
    @DeleteMapping("checkGroup/delete/{id}")
    public Result delete(@PathVariable("id") int id){
        //检查组信息修改 删除套餐详情缓存数据
        RedisUtil.removeKeys(RedisH5Constant.SETMEAL_DETAIL);
        return  new Result(checkGroupService.delete(id));
    }

//    //恢复数据
//    @GetMapping("checkitem/recovery/{id}")
//    public Result recovery(@PathVariable("id") int id){
//    return new Result(checkItemService.recovery(id));
//    }
//
//    //查询被删除的数据
//    @PostMapping("checkitem/findDeletedPage")
//    public Result findDeletedPage(@RequestBody QueryPageBean queryPageBean){
//        return   new Result(checkItemService.findDeletedPage(queryPageBean)); //   total  +  List
//    }
//
//    //恢复选中数据
//    @PostMapping("checkitem/restoreAll")
//    public Result restoreAll(@RequestBody List<CheckItem> list){
//     return new Result(checkItemService.restoreAll(list));
//    }
}








