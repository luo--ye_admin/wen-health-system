package com.wen.health.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wen.health.pojo.CheckItem;

public interface CheckItemMapper extends BaseMapper<CheckItem> {
}
