package com.wen.health.quartzJob;

import com.wen.health.service.OrderSettingService;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ClearExpiredOrderSetting implements Job {

    @Autowired
    OrderSettingService orderSettingService;

    private String name;

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        boolean flag = orderSettingService.clearExpiredOrderSetting();
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        System.out.println("CRON ----> schedule job1 is running ... + " + name + "  ---->  " + dateFormat.format(new Date()) + "执行结果" + flag);

    }
}
