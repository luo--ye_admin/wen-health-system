package com.wen.health.xxljob;

import com.wen.health.service.SetMealService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author 十一
 * @Description
 * @create 2020-11-08-14:29
 */
@JobHandler(value = "heima.setmeal.clean.img.job") //  web配置JobHandler的名称
@Component
public class MyJob extends IJobHandler {
    /**
     * 但是spring的@Scheduled只支持6位，年份是不支持的，带年份的7位格式会报错：
     * Cron expression must consist of 6 fields (found 7 in "1/5 * * * * ? 2018")
     * 通过cron表达式 来配置 该方法的执行周期
     * **/
    @Resource
    private SetMealService setMealService;

    @Override
    public ReturnT<String> execute(String param) throws Exception {
        System.out.println("====清理垃圾图片====");
        setMealService.clearOssImg();
        return SUCCESS;
    }


}
