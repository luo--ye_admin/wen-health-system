package com.wen.health.quartzJob;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class MyStartupRunner implements CommandLineRunner {

    @Autowired
    public CronSchedulerJob scheduleJobs;

    @Override
    public void run(String... args) throws Exception {
        //定期清理过期预约设置数据（order setting）使用quartz框架
        scheduleJobs.scheduleJobs();
        System.out.println(">>>>>>>>>>>>>>>quartz定时任务开始执行<<<<<<<<<<<<<");
    }
}