package com.wen.health.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wen.health.entity.PageResult;
import com.wen.health.entity.QueryPageBean;
import com.wen.health.mapper.CheckItemMapper;
import com.wen.health.pojo.CheckItem;
import com.wen.health.service.CheckItemService;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Service
@Transactional
public class CheckItemServiceImpl extends ServiceImpl<CheckItemMapper, CheckItem> implements CheckItemService {

    //分页查询
    @Override
    public PageResult findPage(QueryPageBean queryPageBean) {
        // 无条件分页
        //Page<CheckItem> page = page(new Page<CheckItem>(queryPageBean.getCurrentPage(), queryPageBean.getPageSize()));
        //除去is_delete = 1 的数据
        QueryWrapper<CheckItem> wrapper = new QueryWrapper<>();
        wrapper.eq("is_delete",0);
        Page<CheckItem> page=null;
        if(StringUtils.isBlank(queryPageBean.getQueryString())){
            //普通分页查询
            page = page(new Page<>(queryPageBean.getCurrentPage(), queryPageBean.getPageSize()),wrapper);
       }else{
            //条件查询
            wrapper.like("name",queryPageBean.getQueryString());
            wrapper.or();
            wrapper.like("code",queryPageBean.getQueryString());
            page = page(new Page<>(queryPageBean.getCurrentPage(), queryPageBean.getPageSize()),wrapper);
        }
        PageResult  result = new PageResult(page.getTotal(),page.getRecords());
        return result;
    }

    //逻辑删除，将用户删除的数据隐藏不显示
    @Override
    public Boolean deleteById(int id) {
        CheckItem checkItem = new CheckItem();
        checkItem.setId(id);
        checkItem.setIs_delete(1);
        return updateById(checkItem);
    }

    //恢复数据
    @Override
    public Boolean recovery(int id) {
        CheckItem checkItem = new CheckItem();
        checkItem.setId(id);
        checkItem.setIs_delete(0);
        return updateById(checkItem);
    }

    //查询被删除的数据
    @Override
    public PageResult findDeletedPage(QueryPageBean queryPageBean) {
        QueryWrapper<CheckItem> wrapper = new QueryWrapper<>();
        wrapper.like("is_delete",1);
        Page<CheckItem> page = page(new Page<>(queryPageBean.getCurrentPage(), queryPageBean.getPageSize()), wrapper);
        return  new PageResult(page.getTotal(),page.getRecords());
    }

    //恢复选中数据
    @Override
    public Object restoreAll(List<CheckItem> list) {
        return updateBatchById(list);
    }
}
