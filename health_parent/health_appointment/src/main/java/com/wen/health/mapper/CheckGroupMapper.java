package com.wen.health.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wen.health.pojo.CheckGroup;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

public interface CheckGroupMapper extends BaseMapper<CheckGroup> {
    //删除中间表信息
    @Delete("delete from t_checkgroup_checkitem where checkgroup_id = #{id}")
    void deleteCheckItemByGroupId(@Param("id") Integer id);

    //向中间表添加数据
    @Insert("insert into t_checkgroup_checkitem values(#{gid},#{checkItemId})")
    void addCheckItemInfo(@Param("gid") Integer gid, @Param("checkItemId") Integer checkItemId);

    //根据检查组主键查询中间表信息
    @Select("select checkitem_id  from t_checkgroup_checkitem where checkgroup_id = #{id}")
    int[] findAllCheckItemByGroupId(@Param("id") int id);
}
