package com.wen.health.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wen.health.dto.SetmealDTO;
import com.wen.health.entity.PageResult;
import com.wen.health.entity.QueryPageBean;
import com.wen.health.mapper.SetMealMapper;
import com.wen.health.pojo.CheckGroup;
import com.wen.health.pojo.CheckItem;
import com.wen.health.pojo.Setmeal;
import com.wen.health.service.CheckGroupService;
import com.wen.health.service.SetMealService;
import com.wen.health.utils.aliyunoss.AliyunUtils;
import com.wen.health.utils.redis.DistributedRedisLock;
import com.wen.health.utils.redis.RedisUtil;
import com.wen.health.utils.resources.RedisConstant;
import com.wen.health.utils.resources.RedisH5Constant;
import com.wen.health.vo.CheckGroupVO;
import com.wen.health.vo.SetmealVO;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.Set;

/**
 * @author 十一
 * @Description
 * @create 2020-11-06-18:53
 */
@Service
@Transactional
public class SetMealServiceImpl extends ServiceImpl<SetMealMapper, Setmeal> implements SetMealService {

    @Autowired
    private CheckGroupService checkGroupService;

    //分页查询
    @Override
    public PageResult findPage(QueryPageBean queryPageBean) {
        //创创建条件构造器
        QueryWrapper<Setmeal> wrapper = new QueryWrapper<>();
        wrapper.eq("is_delete",0);
        Page<Setmeal> page=null;
        if(StringUtils.isBlank(queryPageBean.getQueryString())){
            //普通查询
            page = page(new Page<>(queryPageBean.getCurrentPage(), queryPageBean.getPageSize()),wrapper);
        }else{
            //条件查询
            wrapper.like("name",queryPageBean.getQueryString());
            wrapper.or();
            wrapper.like("code",queryPageBean.getQueryString());
            wrapper.or();
            wrapper.like("helpCode",queryPageBean.getQueryString());
            page=page(new Page<>(queryPageBean.getCurrentPage(),queryPageBean.getPageSize()),wrapper);
        }
        return  new PageResult(page.getTotal(), page.getRecords());
    }

    //新增或修改
    @Override
    public Boolean addOrUpdate(SetmealDTO setmealDTO) {

        //判断是添加操作还是修改操作
        Integer id = setmealDTO.getId();
        if(StringUtils.isNotBlank(String.valueOf(id))){
            //修改操作，删除中间表中的数据
            baseMapper.deleteCheckGroupBySetMealId(id);
            if(setmealDTO.getImg() != null){
                //删除套餐表图片信息
                baseMapper.deleteImgById(id);
            }
        }
        //添加信息到SetMeal表
        boolean flag = saveOrUpdate(setmealDTO);
        Integer mid = setmealDTO.getId();
        //如果gid值存在表示执行的是新增操作gid
        Integer[] checkgroupIds = setmealDTO.getCheckgroupIds();
        if(checkgroupIds != null && checkgroupIds.length > 0){
            for (Integer checkgroupId : checkgroupIds) {
                //想中间表添加数据
                baseMapper.addCheckGroupInfo(mid,checkgroupId);
            }
        }
        //用户添加预约套餐，删除set中同名图片文件
        RedisUtil.removeSetMember(RedisConstant.ALL_SETMEAL_PIC_SET,setmealDTO.getImg());
        //套餐信息修改 删除套餐列表缓存数据
        RedisUtil.removeKeys(RedisH5Constant.ALL_SETMEAL);
        //套餐信息修改 删除套餐详情缓存数据
        RedisUtil.removeKeys(RedisH5Constant.SETMEAL_DETAIL);
        return flag;
    }

    //定时清理垃圾图片--由xxl-job任务调度
    @Override
    public void clearOssImg(){
        //获取Redis中set集合中的所有元素
        Set<String > membersOfSet = RedisUtil.getMembersOfSet(RedisConstant.ALL_SETMEAL_PIC_SET);
        //判断集合是否为空
        if(membersOfSet.size() != 0){
            //遍历集合
            for (String uuidFileName : membersOfSet) {
                //判断Redis中是否存在相同值的key-value
                boolean existsKey = RedisUtil.existsKey(RedisConstant.SINGLE_PIC + uuidFileName);
                System.out.println("existsKey = " + existsKey);
                if(existsKey == false){
                    //删除阿里云中的垃圾图片
                    AliyunUtils.deleteFile(uuidFileName);
                    System.out.println("从阿里云中删除了垃圾图片："+uuidFileName);
                    //删除set集合中的文件
                    RedisUtil.removeSetMember(RedisConstant.ALL_SETMEAL_PIC_SET,uuidFileName);
                }
            }
        }
    }

    //查询套餐详细信息
    @Override
    public SetmealVO findSetMealDetail(int id) {
        //先查询缓存中是否存在数据
        Object object = RedisUtil.get(RedisH5Constant.SETMEAL_DETAIL + id);
        SetmealVO setmealVO = JSON.parseObject(String.valueOf(object), SetmealVO.class);
        //如果不存在
        if (setmealVO == null) {
            //加锁 锁名与缓存命相同 锁失效时间1S
            DistributedRedisLock redisLock = new DistributedRedisLock();
            redisLock.lock(RedisH5Constant.SETMEAL_DETAIL + id, 1);
            try {
                //数据库查询数据
                //1、查询套餐信息
                setmealVO = baseMapper.findSetMealDetail(id);
                //2、查询套餐中的检查组信息
                List<CheckGroupVO> checkGroupVOList = baseMapper.findCheckGroupsBySetMealId(id);
                //3、查询检查组中的检查项信息
                for (CheckGroupVO checkGroupVO : checkGroupVOList) {
                    List<CheckItem> checkItemList = baseMapper.findCheckItemByCheckGroupId(checkGroupVO.getId());
                    checkGroupVO.setCheckItemList(checkItemList);
                }
                setmealVO.setCheckGroups(checkGroupVOList);
                //将查询出来的数据放入redis缓存中 并返回给controller
                RedisUtil.set(RedisH5Constant.SETMEAL_DETAIL + id, setmealVO);
                System.out.println("从数据库获取套餐详情数据");
                return setmealVO;
            } catch (Exception e) {
                throw new RuntimeException("数据查询并放入缓存时出错!Exception:" + e.getMessage());
            } finally {
                redisLock.unlock(RedisH5Constant.SETMEAL_DETAIL + id);
            }
        } else {
            //如果缓存中存在数据 则直接将缓存中的数据返回给controller
            System.out.println("从缓存中获取套餐详情数据");
            return setmealVO;
        }
    }
    //查询套餐基本信息
    @Override
    public Setmeal findSetMealById(int id) {
        return baseMapper.findSetMealById(id);
    }

    //查询套餐检查组信息
    @Override
    public SetmealVO findCheckGroupsBySetMealId(int id) {
        //查询所有套餐
        List<CheckGroup> checkGroupList = checkGroupService.list();
        //查询套餐对应的检查组id
        int[] checkGroupIds = baseMapper.findCheckGroupIdsBySetMealId(id);
        SetmealVO setmealVO = new SetmealVO();
        setmealVO.setCheckGroupList(checkGroupList);
        setmealVO.setCheckGroupIds(checkGroupIds);
        return setmealVO;
    }

    //删除套餐信息
    @Override
    public Boolean deleteSetMeal(int id) {
        //套餐信息修改 删除套餐列表缓存数据
        RedisUtil.removeKeys(RedisH5Constant.ALL_SETMEAL);
        //套餐信息修改 删除套餐详情缓存数据
        RedisUtil.removeKeys(RedisH5Constant.SETMEAL_DETAIL);
        return baseMapper.deleteSetMeal(id);
    }

    //查询所有套餐
    @Override
    public List<Setmeal> findAllSetMeal() {
        //先查询缓存中是否存在数据
        List<Setmeal> allSetmeal = RedisUtil.get(RedisH5Constant.ALL_SETMEAL);
        //如果不存在
        if (allSetmeal == null || allSetmeal.size() == 0) {
            //加锁 锁名与缓存命相同 锁失效时间1S
            DistributedRedisLock redisLock = new DistributedRedisLock();
            redisLock.lock(RedisH5Constant.ALL_SETMEAL, 1);
            try {
                //数据库查询数据
                QueryWrapper<Setmeal> wrapper = new QueryWrapper<>();
                wrapper.eq("is_delete", 0);
                allSetmeal = baseMapper.selectList(wrapper);
                //将查询出来的数据放入redis缓存中 并返回给controller
                RedisUtil.set(RedisH5Constant.ALL_SETMEAL, allSetmeal);
                System.out.println("从数据库获取套餐列表数据");
                return allSetmeal;
            } catch (Exception e) {
                throw new RuntimeException("数据查询并放入缓存时出错!Exception:" + e.getMessage());
            } finally {
                redisLock.unlock(RedisH5Constant.ALL_SETMEAL);
            }
        } else {
            //如果缓存中存在数据 则直接将缓存中的数据返回给controller
            System.out.println("从缓存中获取套餐列表数据");
            return allSetmeal;
        }
    }

}
