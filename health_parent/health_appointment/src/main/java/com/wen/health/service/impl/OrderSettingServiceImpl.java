package com.wen.health.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.codingapi.txlcn.tc.annotation.LcnTransaction;
import com.wen.health.mapper.OrderSettingMapper;
import com.wen.health.pojo.OrderSetting;
import com.wen.health.service.OrderSettingService;
import com.wen.health.utils.date.DateUtils;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * @author 十一
 * @Description
 * @create 2020-11-06-18:53
 */
@Service
@Transactional
public class OrderSettingServiceImpl extends ServiceImpl<OrderSettingMapper, OrderSetting> implements OrderSettingService {

    //存储进数据库
    @Override
    public Boolean importOrderSettings(List<String[]> orderSettingList) {
        //解析表格数据
        List<OrderSetting> orderSettings = traverseList(orderSettingList);
        //判断数据库中是否存在重复数据
        for (OrderSetting orderSetting : orderSettings) {
            QueryWrapper<OrderSetting> wrapper = new QueryWrapper<>();
            wrapper.eq("orderdate",orderSetting.getOrderDate());
            OrderSetting isExist = baseMapper.selectOne(wrapper);
            if(isExist != null){
                //数据库中存在重复数据
                orderSetting.setId(isExist.getId());
                orderSetting.setReservations(isExist.getReservations());
            }
        }
        return saveOrUpdateBatch(orderSettings);
    }

    //查询预约数据
    @Override
    public Map findSettingData(int year, int month) {
        String startDate = year+"-"+month+"-1";
        String endDate = year+"-"+month+"-31";
        List<OrderSetting> orderSettingList = baseMapper.findSettingDataByYearAndMonth(startDate, endDate);
        Map map = new HashMap();
        for (OrderSetting orderSetting : orderSettingList) {
            Date orderDate = orderSetting.getOrderDate();
            String dateString = DateUtils.parseDate2String(orderDate,"yyyy-MM-dd");
            Map orderSettingMap = new HashMap();
            orderSettingMap.put("number",orderSetting.getNumber());
            orderSettingMap.put("reservations",orderSetting.getReservations());
            map.put(dateString,orderSettingMap);
        }
        return map;
    }

    //修改最大预约数
    @Override
    public Boolean updateNumberByOrderDate(int number, String orderDate) {
        return  baseMapper.updateNumberByOrderDate(number,orderDate);
    }

    //判断当前时间是否可以预约
    @Override
    public int isOrderOK(String orderDate) {
        return baseMapper.isOrderOK(orderDate);
    }

    //预约成功，更新表数据
    @Override
    @LcnTransaction
    public void updateReservationByOrderDate(String orderDate) {
        baseMapper.updateReservationsByOrderDate(orderDate);
    }
    /**
     * 预约数-1
     * @param orderDate
     * @author liusen
     */
    @Override
    @LcnTransaction
    public void updateReservationByOrderDateDec(String orderDate) {
        baseMapper.updateReservationByOrderDateDec(orderDate);
    }


    //遍历集合将信息封装进实体类
    private List<OrderSetting>  traverseList(List<String[]> orderSettingList){
        //判断集合是否为空
        if(orderSettingList != null && orderSettingList.size() > 0){
            List<OrderSetting>  orderSettings = new ArrayList<OrderSetting>();
            for (String[] strings : orderSettingList) {
                OrderSetting orderSetting = new OrderSetting();
                orderSetting.setReservations(0);
                orderSetting.setNumber(Integer.valueOf(strings[1]));
                orderSetting.setOrderDate(DateUtils.parseString2Date(strings[0],"yyyy/MM/dd"));
                orderSettings.add(orderSetting);
            }
            return orderSettings;
        }
        return  null;
    }

    //定期清理过期预约设置数据（order setting）使用quartz框架
    public boolean clearExpiredOrderSetting() {
        return baseMapper.clearExpiredOrderSetting();
    }

}
