package com.wen.health.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wen.health.pojo.CheckItem;
import com.wen.health.pojo.Setmeal;
import com.wen.health.vo.CheckGroupVO;
import com.wen.health.vo.SetmealVO;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author 十一
 * @Description
 * @create 2020-11-07-20:04
 */
public interface SetMealMapper extends BaseMapper<Setmeal> {
    //删除中间表数据
    @Delete("delete from t_setmeal_checkgroup where setmeal_id = #{id}")
    void deleteCheckGroupBySetMealId(@Param("id") Integer id);

    //向中间表添加数据
    @Insert("insert into t_setmeal_checkgroup values(#{mid},#{checkgroupId}) ")
    void addCheckGroupInfo(@Param("mid") Integer mid, @Param("checkgroupId") Integer checkgroupId);

    //查询预约套餐信息
    @Select("select name , age , remark , img , sex from t_setmeal where id = #{id}")
    SetmealVO findSetMealDetail(@Param("id") int id);

    //查询预约套餐中的检查组信息
    @Select("select tc.id, tc.name ,tc.remark from t_checkgroup tc left join t_setmeal_checkgroup tsc on tsc.CHECKGROUP_ID = tc.ID where tsc.SETMEAL_ID = #{id}")
    List<CheckGroupVO> findCheckGroupsBySetMealId(@Param("id") int id);

    //查询检查组中的检查项信息
    @Select("SELECT tc.name  " +
            "FROM  t_checkgroup_checkitem tcc , t_checkitem tc " +
            "WHERE tcc.CHECKITEM_ID = tc.ID " +
            "and tcc.CHECKGROUP_ID = #{id}")
    List<CheckItem> findCheckItemByCheckGroupId(@Param("id") Integer id);

    //查询单个套餐基本信息
    @Select("select name , remark , age , sex from t_setmeal where id = #{id}")
    Setmeal findSetMealById(@Param("id") int id);

    //查询套餐对应的检查组id
    @Select("select tsc.CHECKGROUP_ID from t_setmeal_checkgroup tsc where tsc.SETMEAL_ID = #{id} ")
    int[] findCheckGroupIdsBySetMealId(@Param("id") int id);

    //删除套餐表图片
    @Update("update t_setmeal set img = '' where id = #{id}")
    void deleteImgById(@Param("id") Integer id);

    //逻辑删除套餐信息
    @Update("update t_setmeal set is_delete = 1 where id =#{id}")
    Boolean deleteSetMeal(@Param("id") int id);
}
