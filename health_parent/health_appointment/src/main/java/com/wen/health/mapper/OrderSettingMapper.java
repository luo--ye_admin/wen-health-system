package com.wen.health.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wen.health.pojo.OrderSetting;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * @author 十一
 * @Description
 * @create 2020-11-09-19:51
 */
public interface OrderSettingMapper extends BaseMapper<OrderSetting> {
    @Select("select  ORDERDATE,NUMBER,RESERVATIONS from t_ordersetting where  orderdate between #{startDate} and #{endDate}")
    List<OrderSetting> findSettingDataByYearAndMonth(@Param("startDate") String startDate, @Param("endDate") String endDate);

    @Update("update t_ordersetting  set number= #{number}  where  orderdate= #{orderDate}")
    Boolean updateNumberByOrderDate(@Param("number") int number, @Param("orderDate") String orderDate);

    @Select("select count(1) from t_ordersetting where number >RESERVATIONS and orderdate = #{orderDate}")
    int isOrderOK(@Param("orderDate") String orderDate);

    @Update("update t_ordersetting set reservations = reservations + 1 where orderdate = #{orderDate}")
    void updateReservationsByOrderDate(@Param("orderDate") String orderDate);

    @Update("update t_ordersetting set reservations = reservations - 1 where orderdate = #{orderDate}")
    void updateReservationByOrderDateDec(String orderDate);


    @Delete("DELETE FROM t_ordersetting WHERE MONTH(ORDERDATE) = MONTH(NOW()) - 1")
    boolean clearExpiredOrderSetting();
}
