package com.wen.health.quartzJob;

import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.stereotype.Component;

@Component
public class CronSchedulerJob {

    @Autowired
    private SchedulerFactoryBean schedulerFactoryBean;

    private void scheduleJob1(Scheduler scheduler) throws SchedulerException {
        JobDetail jobDetail = JobBuilder.newJob(ClearExpiredOrderSetting.class) .withIdentity("job1", "group1").build();
        // 每月最后一天凌晨2点执行一次执行清理任务，自动清理上个月的预约设置数据
//        CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule("0 0 2 L * ?");
        //测试：11：20执行一次 清理八月份数据
        CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule("0 55 11 * * ?");
        CronTrigger cronTrigger = TriggerBuilder.newTrigger().withIdentity("trigger1", "group1")
                .usingJobData("name","lvcy").withSchedule(scheduleBuilder).build();
        scheduler.scheduleJob(jobDetail,cronTrigger);
    }

    public void scheduleJobs() throws SchedulerException {
        Scheduler scheduler = schedulerFactoryBean.getScheduler();
        scheduleJob1(scheduler);
    }
}