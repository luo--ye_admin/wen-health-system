package com.wen.health.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wen.health.dto.CheckGroupDTO;
import com.wen.health.entity.PageResult;
import com.wen.health.entity.QueryPageBean;
import com.wen.health.mapper.CheckGroupMapper;
import com.wen.health.pojo.CheckGroup;
import com.wen.health.pojo.CheckItem;
import com.wen.health.service.CheckGroupService;
import com.wen.health.service.CheckItemService;
import com.wen.health.vo.CheckGroupVO;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author 十一
 * @Description
 * @create 2020-11-06-18:53
 */
@Service
@Transactional
public class CheckGroupServiceImpl extends ServiceImpl<CheckGroupMapper, CheckGroup> implements CheckGroupService {

    @Autowired
    private CheckItemService checkItemService;

    //分页查询
    @Override
    public PageResult findPage(QueryPageBean queryPageBean) {
        //创创建条件构造器
        QueryWrapper<CheckGroup> wrapper = new QueryWrapper<>();
        wrapper.eq("is_delete",0);
        Page<CheckGroup> page=null;
        if(StringUtils.isBlank(queryPageBean.getQueryString())){
            //普通查询
            page = page(new Page<>(queryPageBean.getCurrentPage(), queryPageBean.getPageSize()),wrapper);
        }else{
            //条件查询
            wrapper.like("name",queryPageBean.getQueryString());
            wrapper.or();
            wrapper.like("code",queryPageBean.getQueryString());
            wrapper.or();
            wrapper.like("helpCode",queryPageBean.getQueryString());
            page=page(new Page<>(queryPageBean.getCurrentPage(),queryPageBean.getPageSize()),wrapper);
        }
        PageResult pageResult = new PageResult(page.getTotal(), page.getRecords());
        return pageResult;
    }

    //新增或修改
    @Override
    public Boolean addOrUpdate(CheckGroupDTO checkGroupDTO) {
        //1、判断是添加操作还是修改操作
        Integer id = checkGroupDTO.getId();
        if(StringUtils.isNotBlank(String.valueOf(id))){
            //修改操作，删除中间表中的数据
            baseMapper.deleteCheckItemByGroupId(id);
        }
        //添加信息到checkGroup表
        boolean flag = saveOrUpdate(checkGroupDTO);
        Integer gid = checkGroupDTO.getId();
        //如果gid值存在表示执行的是新增操作gid
        Integer[] checkItemIds = checkGroupDTO.getCheckitemIds();
        if(checkItemIds != null && checkItemIds.length > 0){
            for (Integer checkItemId : checkItemIds) {
                //想中间表添加数据
                baseMapper.addCheckItemInfo(gid,checkItemId);
            }
        }
        return flag;
    }

    //查询编辑表单中的检查项数据
    @Override
    public CheckGroupVO findAllCheckItemByGroupId(int id) {
        List<CheckItem> list = checkItemService.list();
        //根据检查组主键查询中间表的数据
        int[] checkItemIds = baseMapper.findAllCheckItemByGroupId(id);
        CheckGroupVO checkGroupVO = new CheckGroupVO();
        checkGroupVO.setCheckItemList(list);
        checkGroupVO.setCheckitemIds(checkItemIds);
        return checkGroupVO;
    }

    //逻辑删除检查组信息
    @Override
    public Boolean delete(int id) {
        CheckGroup checkGroup = new CheckGroup();
        checkGroup.setId(id);
        checkGroup.setIs_delete(1);
        boolean flag = updateById(checkGroup);
        return flag;
    }
}
