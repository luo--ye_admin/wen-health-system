package com.wen.health.vo;


import com.wen.health.pojo.User;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class UserVO extends User {

    private List<RoleVO> roles = new ArrayList<>();//对应角色集合

}
