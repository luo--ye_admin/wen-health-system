package com.wen.health.vo;


import com.wen.health.pojo.CheckGroup;
import com.wen.health.pojo.Setmeal;
import lombok.Data;

import java.util.List;

@Data
public class SetmealVO extends Setmeal {

    private List<CheckGroupVO> checkGroups;

    private List<CheckGroup> checkGroupList;

    private int[] checkGroupIds;

}
