package com.wen.health.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wen.health.pojo.OrderSetting;

import java.util.List;
import java.util.Map;

/**
 * @author 十一
 * @Description
 * @create 2020-11-09-19:41
 */
public interface OrderSettingService extends IService<OrderSetting> {

    Boolean importOrderSettings(List<String[]> orderSettingList);

    Map findSettingData(int year, int month);

    Boolean updateNumberByOrderDate(int number, String orderDate);

    int isOrderOK(String orderDate);

    void updateReservationByOrderDate(String orderDate);

    void updateReservationByOrderDateDec(String orderDate);

    boolean clearExpiredOrderSetting();
}
