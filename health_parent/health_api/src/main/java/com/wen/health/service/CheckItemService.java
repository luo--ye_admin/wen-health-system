package com.wen.health.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wen.health.entity.PageResult;
import com.wen.health.entity.QueryPageBean;
import com.wen.health.pojo.CheckItem;

import java.util.List;

public interface CheckItemService extends IService<CheckItem> {
    PageResult findPage(QueryPageBean queryPageBean); //  分页查询

    Boolean deleteById(int id);

    Boolean recovery(int id);

    PageResult findDeletedPage(QueryPageBean queryPageBean);

    Object restoreAll(List<CheckItem> list);

}
