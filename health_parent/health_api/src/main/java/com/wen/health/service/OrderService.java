package com.wen.health.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wen.health.pojo.Order;

import java.util.List;
import java.util.Map;

/**
 * @author 十一
 * @Description
 * @create 2020-11-12-19:52
 */
public interface OrderService extends IService<Order> {
    Order add(Map map);

    Map findOrderInfoByOrderId(int id);

    boolean isExistOrderId(String oid);

    String findPhonenumberByOid(String oid);

    void updateStatus(String oid);

    Integer findPayStatus(String oid);

    void deleteOrderByOrderId(String oid);

    void clearOrder();

    Integer findTodayOrderNumber();

    Integer findTodayVisitsNumber();

    Integer findThisWeekOrderNumber();

    Integer findThisWeekVisitsNumber();

    Integer findThisMonthOrderNumber();

    Integer findThisMonthVisitsNumber();

    List<Map<String, Object>> findHotSetmeal();
}
