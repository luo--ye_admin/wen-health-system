package com.wen.health.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class ReportVo implements Serializable {
//    private  List<Map> mapList;
    private List<Long> memberCounts;
    private List<String> monthList;
}
