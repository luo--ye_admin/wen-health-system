package com.wen.health.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wen.health.pojo.Member;
import java.util.List;
import java.util.Map;


/**
 * @author 十一
 * @Description
 * @create 2020-11-12-20:32
 */
public interface MemberService extends IService<Member> {

    //查询是否存在当前用户
    Member findMemberByTelephone(String telephone);

    int saveMember(Member member);

    List<Map> findMemberProportionByAge();

    List<Map> findMemberProportionBySex();

    Integer findTodayNewMember();

    Integer findTotalMember();

    Integer findThisWeekNewMember();

    Integer findThisMonthNewMember();
}
