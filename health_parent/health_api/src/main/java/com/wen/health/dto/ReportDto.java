package com.wen.health.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class ReportDto implements Serializable {
    private String  begin;
    private String  end;
}
