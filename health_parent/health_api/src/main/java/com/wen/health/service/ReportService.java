package com.wen.health.service;

import com.wen.health.dto.ReportDto;
import com.wen.health.vo.ReportVo;

import java.util.List;
import java.util.Map;

public interface ReportService {
    List<Map> setmealProportion();

    ReportVo memberReportByLastYear(ReportDto reportDto);

    Map findBusinessReportData();
}
