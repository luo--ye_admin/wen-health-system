package com.wen.health.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wen.health.entity.PageResult;
import com.wen.health.entity.QueryPageBean;
import com.wen.health.pojo.Permission;

public interface PermissionService extends IService<Permission> {

    PageResult findPage(QueryPageBean queryPageBean);

    Boolean deletePermissionAndRoleByPid(int id);
}
