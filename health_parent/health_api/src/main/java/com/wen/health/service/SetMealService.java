package com.wen.health.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wen.health.dto.SetmealDTO;
import com.wen.health.entity.PageResult;
import com.wen.health.entity.QueryPageBean;
import com.wen.health.pojo.Setmeal;
import com.wen.health.vo.SetmealVO;

import java.util.List;

/**
 * @author 十一
 * @Description
 * @create 2020-11-07-20:01
 */
public interface SetMealService extends IService<Setmeal> {

    //套餐管理分页查询
    PageResult findPage(QueryPageBean queryPageBean);

    //添加或修改
    Boolean addOrUpdate(SetmealDTO setmealDTO);

    //定时清理垃圾图片
    void clearOssImg();

    //查询套餐详细信息
    SetmealVO findSetMealDetail(int id);

    //查询套餐基本信息
    Setmeal findSetMealById(int id);

    //查询套餐检查组信息
    SetmealVO findCheckGroupsBySetMealId(int id);

    //逻辑删除套餐信息
    Boolean deleteSetMeal(int id);

    //查询所有套餐
    List<Setmeal> findAllSetMeal();
}
