package com.wen.health.dto;

import com.wen.health.pojo.Role;
import lombok.Data;

// DTO : Data Transfer Object : 数据传输对象
//后端pojo中的实体类不满足前端请求的数据时(前端发来的数据除了实体中的字段还有其他的字段,这里是checkitemIds数组),
// 可以创建一个DTO 用于处理这种业务
@Data
public class RoleDTO extends Role {
//前端传过来的数据包含检查项的id数组,这里创建一个DTO对象用来接收,继承CheckGroup里面所有的字段
/**
* 选择的检查项id列表
* */
private Integer[] permissions;

    public Integer[] menus;



}
