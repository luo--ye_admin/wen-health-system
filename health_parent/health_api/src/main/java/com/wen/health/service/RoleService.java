package com.wen.health.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.wen.health.dto.RoleDTO;
import com.wen.health.entity.PageResult;
import com.wen.health.entity.QueryPageBean;
import com.wen.health.pojo.Role;
import com.wen.health.vo.RoleVO;

public interface RoleService extends IService<Role> {

    PageResult findPage(QueryPageBean queryPageBean);

    Boolean add(RoleDTO roleDTO);

    RoleVO findPermissionInfoByRoleId(Integer id);

    Boolean deleteRoleById(int id);

    RoleVO findMenuInfoByRoleId(Integer id);
}
