package com.wen.health.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wen.health.dto.CheckGroupDTO;
import com.wen.health.entity.PageResult;
import com.wen.health.entity.QueryPageBean;
import com.wen.health.pojo.CheckGroup;
import com.wen.health.vo.CheckGroupVO;

/**
 * @author 十一
 * @Description
 * @create 2020-11-06-18:53
 */
public interface CheckGroupService extends IService<CheckGroup> {
    PageResult findPage(QueryPageBean queryPageBean);

    Boolean addOrUpdate(CheckGroupDTO checkGroupDTO);

    CheckGroupVO findAllCheckItemByGroupId(int id);

    Boolean delete(int id);
    //分页查询
}
