package com.wen.health.vo;


import com.wen.health.pojo.Menu;
import com.wen.health.pojo.Permission;
import com.wen.health.pojo.Role;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class RoleVO extends Role {

    private List<Permission> permissionList = new ArrayList<>();//对应权限集合
    private List<MenuVO> menuVOList = new ArrayList<>();
    private int[] permissions;   //权限的id
    private int[] menus;
    private List<Menu>menuList;
}
