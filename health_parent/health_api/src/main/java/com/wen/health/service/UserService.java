package com.wen.health.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wen.health.entity.PageResult;
import com.wen.health.entity.QueryPageBean;
import com.wen.health.pojo.User;
import com.wen.health.vo.UserVO;

/**
 * @author 十一
 * @Description
 * @create 2020-11-14-15:45
 */
public interface UserService extends IService<User> {

    UserVO findUserDetailByUsername(String username);

    PageResult findPage(QueryPageBean queryPageBean);

    int findUserByUsername(String username);
}
