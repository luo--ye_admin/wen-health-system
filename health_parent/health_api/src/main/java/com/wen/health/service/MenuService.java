package com.wen.health.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wen.health.entity.PageResult;
import com.wen.health.entity.QueryPageBean;
import com.wen.health.pojo.Menu;

import java.util.List;

public interface MenuService extends IService<Menu> {
    List findMenuByUsername(String username);

    PageResult findPage(QueryPageBean queryPageBean);

    List findAll();


    Boolean deleteMenuByMId(int id);
}
