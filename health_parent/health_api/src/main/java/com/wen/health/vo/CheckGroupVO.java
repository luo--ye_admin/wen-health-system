package com.wen.health.vo;


import com.wen.health.pojo.CheckGroup;
import com.wen.health.pojo.CheckItem;
import lombok.Data;

import java.util.Arrays;
import java.util.List;

@Data
public class CheckGroupVO extends CheckGroup {

    private List<CheckItem> checkItemList;

    private int[] checkitemIds;

    @Override
    public String toString() {
        return "CheckGroupVO{" +
                "checkItemList=" + checkItemList +
                ", checkitemIds=" + Arrays.toString(checkitemIds) +
                '}';
    }
}
