package com.wen.health;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author 十一
 * @Description
 * @create 2020-11-14-15:46
 */
@SpringBootApplication
@MapperScan(basePackages = "com.itheima.health.mapper")
public class SystemApplication {
    public static void main(String[] args) {
        SpringApplication.run(SystemApplication.class,args);
        System.out.println("---系统服务启动---");
    }
}
