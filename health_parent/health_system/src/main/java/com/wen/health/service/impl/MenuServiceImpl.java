package com.wen.health.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wen.health.entity.PageResult;
import com.wen.health.entity.QueryPageBean;
import com.wen.health.mapper.MenuMapper;
import com.wen.health.pojo.Menu;
import com.wen.health.service.MenuService;
import com.wen.health.vo.MenuVO;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class MenuServiceImpl extends ServiceImpl<MenuMapper, Menu> implements MenuService {

    @Override
    public List findMenuByUsername(String username) {
        int id = baseMapper.findRolerByUsername(username);
        List<MenuVO> menuVoList = baseMapper.findMenuByRoleId(id);
//        System.out.println("menuVoList = " + menuVoList);
        List<MenuVO>menuChildren= new  ArrayList<>();//所有的子菜单
        List<MenuVO>menuParent=new ArrayList<>();//父菜单
        for (MenuVO menu : menuVoList) {
            String path = menu.getPath();
            if(path.isEmpty()){
                menuVoList.remove(menu);
            }else{
                 if(path.startsWith("/")){
                    menuChildren.add(menu);
                                     }else{
                  menuParent.add(menu);
              }
            }
        }
     //父菜单的子菜单集合
        for (MenuVO menu : menuParent) {
            List<MenuVO>menuVOList=new ArrayList<>();
            String pathParent = menu.getPath();
            for (MenuVO menuChild : menuChildren) {
                String pathChild = menuChild.getPath();
                int last = pathChild.lastIndexOf("/");
                int end = pathChild.lastIndexOf("-");
                String path = pathChild.substring(last + 1, end);
                if(path.equalsIgnoreCase(pathParent)){
                    menuVOList.add(menuChild);
                }
            }
            menu.setChildren(menuVOList);
        }
        System.out.println("menuParent = " + menuParent);
        return menuParent ;
    }

    @Override
    public PageResult findPage(QueryPageBean queryPageBean) {
        QueryWrapper<Menu> wrapper = new QueryWrapper<>();
        Page<Menu> page=null;
        if(StringUtils.isBlank(queryPageBean.getQueryString())){
            //普通分页查询
            page = page(new Page<>(queryPageBean.getCurrentPage(), queryPageBean.getPageSize()));
        }else{
            //执行条件分页查询
            wrapper.like("name",queryPageBean.getQueryString());
            page = page(new Page<>(queryPageBean.getCurrentPage(), queryPageBean.getPageSize()),wrapper);
        }
        PageResult result = new PageResult(page.getTotal(),page.getRecords());
        return result;
    }

    @Override
    public List findAll() {
        List<MenuVO> MenuVoList = baseMapper.findAll();
        System.out.println("list = " + MenuVoList);
        List<MenuVO>menuChildren= new  ArrayList<>();//所有的子菜单
        List<MenuVO>menuParent=new ArrayList<>();//父菜单
        for (int i = 0; i < MenuVoList.size() - 1; i++) {
            MenuVO menuVO = MenuVoList.get(i);
            String path = menuVO.getPath();

            if(path==null){
                MenuVoList.remove(menuVO);
            }else{
                 if(path.startsWith("/")){
                menuChildren.add(menuVO);
                 }else{
                 menuParent.add(menuVO);
                  }
             }
        }

        //父菜单的子菜单集合
        for (MenuVO menu : menuParent) {
            List<MenuVO>menuVOList=new ArrayList<>();
            String pathParent = menu.getPath();
            for (MenuVO menuChild : menuChildren) {
                String pathChild = menuChild.getPath();
                int last = pathChild.lastIndexOf("/");
                int end = pathChild.lastIndexOf("-");
                String path = pathChild.substring(last + 1, end);
                if(path.equalsIgnoreCase(pathParent)){
                    menuVOList.add(menuChild);
                   }
            }
            menu.setChildren(menuVOList);
        }
        return menuParent;
    }

    @Override
    public Boolean deleteMenuByMId(int id) {
        int menuAndRoleByPid = baseMapper.findMenuAndRoleByPid(id);
        if(menuAndRoleByPid!=0) {
            baseMapper.deleteMenuByMId(id);
        }
        this.removeById(id);
        return false;
    }

}

