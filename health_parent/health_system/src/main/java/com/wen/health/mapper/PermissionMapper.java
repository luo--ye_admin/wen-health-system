package com.wen.health.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wen.health.pojo.Permission;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

public interface PermissionMapper extends BaseMapper<Permission> {
    @Delete("delete t_role_permission where permission_id= #{id}")
    Boolean deletePermissionAndRoleByPid(@Param("id") Integer id);

    @Select("select count(1) from t_role_permission where permission_id= #{id}")
    int  findPermissionAndRoleByPid(@Param("id") Integer id);
}
