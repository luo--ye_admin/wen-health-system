package com.wen.health.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wen.health.dto.RoleDTO;
import com.wen.health.entity.PageResult;
import com.wen.health.entity.QueryPageBean;
import com.wen.health.mapper.RoleMapper;
import com.wen.health.pojo.Menu;
import com.wen.health.pojo.Permission;
import com.wen.health.pojo.Role;
import com.wen.health.service.MenuService;
import com.wen.health.service.PermissionService;
import com.wen.health.service.RoleService;
import com.wen.health.vo.RoleVO;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.Reference;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;


@Service
@Transactional
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements RoleService {

    @Reference
    private PermissionService permissionService;

    @Autowired
    private MenuService menuService;

    @Override
    public PageResult findPage(QueryPageBean queryPageBean) {
        Page<Role> page = null;
        //这里需要用到mybatisPlus中的QueryWrapper对象
        QueryWrapper<Role> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("is_delete", 0);
        if (StringUtils.isBlank(queryPageBean.getQueryString())) {
            // 无条件分页,如果前端没有传来分页的条件就走这里

            page = page(new Page<Role>(queryPageBean.getCurrentPage(), queryPageBean.getPageSize()), queryWrapper);
        } else {
            //有条件的分页查询

            //like :相当于like %?%  模糊查询, name: 对应数据库的name字段,想让前端分页条件以哪个字段为基准就写哪个字段  这里用name
            //queryPageBean.getQueryString() : 获取到前端带过来的分页条件
            queryWrapper.like("name", queryPageBean.getQueryString());
            //实例Page对象时 将 条 件 追 加 到 最 后
            page = page(new Page<Role>(queryPageBean.getCurrentPage(), queryPageBean.getPageSize()), queryWrapper);
        }
        PageResult result = new PageResult(page.getTotal(), page.getRecords());
        return result;
    }

    @PostMapping("role/add")
    public Boolean add(@RequestBody RoleDTO roleDTO) {
        Integer id = roleDTO.getId();
        if (StringUtils.isNotBlank(String.valueOf(id))) {
            //有id代表是修改操作,删除数据库的数据再添加进去
            baseMapper.deleteRorpById(id);//删除 t_role_permission 中间表
            baseMapper.deleteMenuByMId(id);//删除菜单的主键
        }
        boolean flag = saveOrUpdate(roleDTO);
        Integer rid = roleDTO.getId();//mabatisplus执行操作完成后主键id会被自动赋值,这里直接拿到
        //录入中间表
        Integer[] Permissions = roleDTO.getPermissions();//拿到检查项的所有id
        Integer[] Menus = roleDTO.getMenus(); //拿到菜单项的所有id

        if (Permissions != null && Permissions.length > 0 &Menus != null && Menus.length > 0) {
            //中间表循环录入
            for (Integer Permission : Permissions) {
                baseMapper.addRoleIdAndPermissionId(rid, Permission);
            }
            for(Integer Menu:Menus){
                baseMapper.addRoleIdAndMenusId(rid,Menu);
            }
        }
        return flag;

    }

    @Override
    public RoleVO findPermissionInfoByRoleId(Integer id) {
        List<Permission> list = permissionService.list();
        int[] permissionInfoByRoleId = baseMapper.findPermissionInfoByRoleId(id);
        RoleVO roleVO = new RoleVO();
        roleVO.setPermissions(permissionInfoByRoleId);
        roleVO.setPermissionList(list);

        return roleVO;
    }

    @Override
    public RoleVO findMenuInfoByRoleId(Integer id){
        List<Menu> list = menuService.list();
        int[] menuInfoByRoleId = baseMapper.findMenuInfoByRoleId(id);
        RoleVO roleVO = new RoleVO();
        roleVO.setMenus(menuInfoByRoleId);
        roleVO.setMenuList(list);

        return roleVO;
    }

    @Override
    public Boolean deleteRoleById(int id) {
        Role role = new Role();
        role.setId(id);
        role.setIs_delete(1);
        return updateById(role);
    }
}
