package com.wen.health.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wen.health.pojo.Menu;
import com.wen.health.vo.MenuVO;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface MenuMapper extends BaseMapper<Menu> {

    @Select("select tur.role_id " +
            "from t_user tu, t_user_role tur " +
            "where tu.id=tur.user_id " +
            "and tu.username = #{username} ")
    int  findRolerByUsername(@Param("username") String username);

    @Select("select tm.`NAME`,tm.PATH,tm.icon,tm.LINKURL " +
            "from t_menu tm, t_role_menu trm " +
            "where tm.ID=trm.MENU_ID " +
            "and trm.ROLE_ID=#{id}")
    List<MenuVO> findMenuByRoleId(@Param("id") Integer id);

    @Select("select *  from t_menu ")
    List<MenuVO> findAll();

    @Delete("delete t_role_menu where MENU_ID= #{id}")
    Boolean deleteMenuByMId(@Param("id") Integer id);

    @Select("select count(1) from t_role_menu where MENU_ID= #{id}")
    int findMenuAndRoleByPid(int id);
}
