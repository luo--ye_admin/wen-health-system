package com.wen.health.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wen.health.pojo.Role;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

public interface RoleMapper extends BaseMapper<Role> {

    @Select("select permission_id from t_role_permission where role_id=#{id}")
    int[] findPermissionInfoByRoleId(@Param("id") int id);
    
    @Insert("insert into t_role_permission values(#{rid},#{Permission})")
    void addRoleIdAndPermissionId(@Param("rid") Integer rid, @Param("Permission") Integer Permission);

    @Delete("delete from t_role_permission where role_id = #{rid}")
    void deleteRorpById(@Param("rid") Integer rid);//这里的修改直接删除中间表数据就可以

    @Delete("delete from t_role_menu where role_id= #{id}")
    Boolean deleteMenuByMId(@Param("id") Integer id);

    @Insert("insert into t_role_menu values(#{rid},#{Menu})")
    void addRoleIdAndMenusId(@Param("rid") Integer rid, @Param("Menu")Integer Menu);

    @Select("select menu_id from t_role_menu where role_id=#{id}")
    int[] findMenuInfoByRoleId(@Param("id") Integer id);
}
