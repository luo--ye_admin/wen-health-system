package com.wen.health.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wen.health.pojo.Permission;
import com.wen.health.pojo.User;
import com.wen.health.vo.RoleVO;
import com.wen.health.vo.UserVO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author 十一
 * @Description
 * @create 2020-11-14-15:50
 */
public interface UserMapper extends BaseMapper<User> {
    @Select("select username , password , id from t_user where username = #{username}")
    UserVO finsUserByUsername(@Param("username") String username);

    @Select("select tr.keyword , tr.id " +
            " from t_user_role tur , t_role tr " +
            " where tr.id = tur.role_id and tur.user_id = #{id}")
    List<RoleVO> findUserRolesByUserId(@Param("id") Integer id);

    @Select("SELECT tp.keyword " +
            "FROM t_permission tp , t_role_permission trp  " +
            "WHERE tp.id = trp.permission_id " +
            "and trp.role_id = #{id}")
    List<Permission> findPermissionByRoleId(@Param("id") Integer id);

    @Select("select count(1) from t_user where username = #{username}")
    int  findUserByUsername(String username);
}
