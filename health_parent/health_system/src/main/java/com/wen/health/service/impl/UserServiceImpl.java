package com.wen.health.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wen.health.entity.PageResult;
import com.wen.health.entity.QueryPageBean;
import com.wen.health.mapper.UserMapper;
import com.wen.health.pojo.Permission;
import com.wen.health.pojo.User;
import com.wen.health.service.UserService;
import com.wen.health.vo.RoleVO;
import com.wen.health.vo.UserVO;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author 十一
 * @Description
 * @create 2020-11-14-15:49
 */
@Service
@Transactional
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Override
    public UserVO findUserDetailByUsername(String username) {
        //查询用户基本信息
        UserVO userVO = baseMapper.finsUserByUsername(username);
        //查询用户的所有角色信息
        List<RoleVO> roleVOList = baseMapper.findUserRolesByUserId(userVO.getId());
        //遍历角色集合查询角色对应的角色信息
        for (RoleVO roleVO : roleVOList) {
            List<Permission> permissionList = baseMapper.findPermissionByRoleId(roleVO.getId());
            roleVO.setPermissionList(permissionList);
        }
        userVO.setRoles(roleVOList);
        return userVO;
    }

    @Override
    public PageResult findPage(QueryPageBean queryPageBean) {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        Page<User> page=null;
        if(StringUtils.isBlank(queryPageBean.getQueryString())){
            //普通分页查询
            page = page(new Page<>(queryPageBean.getCurrentPage(), queryPageBean.getPageSize()));
        }else{
            //执行条件分页查询
            wrapper.like("username",queryPageBean.getQueryString());
            page = page(new Page<>(queryPageBean.getCurrentPage(), queryPageBean.getPageSize()),wrapper);
        }
        PageResult result = new PageResult(page.getTotal(),page.getRecords());
        return result;
    }

    @Override
    public int findUserByUsername(String username) {
        int userByUsername = baseMapper.findUserByUsername(username);
        return userByUsername;
    }
}
