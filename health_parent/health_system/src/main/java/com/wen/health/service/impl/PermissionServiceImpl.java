package com.wen.health.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wen.health.entity.PageResult;
import com.wen.health.entity.QueryPageBean;
import com.wen.health.mapper.PermissionMapper;
import com.wen.health.pojo.Permission;
import com.wen.health.service.PermissionService;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class PermissionServiceImpl extends ServiceImpl<PermissionMapper, Permission> implements PermissionService {

    @Override
    public PageResult findPage(QueryPageBean queryPageBean) {
        QueryWrapper<Permission> wrapper = new QueryWrapper<>();
        Page<Permission> page=null;
        if(StringUtils.isBlank(queryPageBean.getQueryString())){
            //普通分页查询
            page = page(new Page<>(queryPageBean.getCurrentPage(), queryPageBean.getPageSize()));
        }else{
            //执行条件分页查询
            wrapper.like("name",queryPageBean.getQueryString());
            page = page(new Page<>(queryPageBean.getCurrentPage(), queryPageBean.getPageSize()),wrapper);
        }
        PageResult result = new PageResult(page.getTotal(),page.getRecords());
        return result;
    }

    @Override
    public Boolean deletePermissionAndRoleByPid(int id) {
        int permissionAndRoleByPid = baseMapper.findPermissionAndRoleByPid(id);
        if(permissionAndRoleByPid!=0){
           baseMapper.deletePermissionAndRoleByPid(id);
        }
       this.removeById(id);
        return false;
    }

}
