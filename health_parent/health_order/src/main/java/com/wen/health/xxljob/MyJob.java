package com.wen.health.xxljob;

import com.wen.health.service.OrderService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author Jason
 * @version 1.0.0
 * @date 2020/11/18 14:09
 * @Description TODO
 */
@JobHandler(value = "heima.health.clean.order.job") //  web配置JobHandler的名称
@Component
public class MyJob extends IJobHandler {
    /**
     * 但是spring的@Scheduled只支持6位，年份是不支持的，带年份的7位格式会报错：
     * Cron expression must consist of 6 fields (found 7 in "1/5 * * * * ? 2018")
     * 通过cron表达式 来配置 该方法的执行周期
     * **/
    @Resource
    private OrderService orderService;

    @Override
    public ReturnT<String> execute(String param) throws Exception {
        System.out.println("---- job清理订单任务调度执行 ----");
        // 调用调度器执行的方法
        orderService.clearOrder();
        return SUCCESS;
    }
}
