package com.wen.health.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.codingapi.txlcn.tc.annotation.LcnTransaction;
import com.wen.health.mapper.OrderMapper;
import com.wen.health.pojo.Member;
import com.wen.health.pojo.Order;
import com.wen.health.service.MemberService;
import com.wen.health.service.OrderService;
import com.wen.health.service.OrderSettingService;
import com.wen.health.utils.date.DateUtils;
import com.wen.health.utils.pay.SnowflakeIdWorker;
import com.wen.health.utils.redis.RedisUtil;
import com.wen.health.utils.resources.RedisConstant;
import com.wen.health.utils.resources.RedisMessageConstant;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.Reference;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;


/**
 * @author 十一
 * @Description
 * @create 2020-11-06-18:53
 */
@Service
@Transactional
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order> implements OrderService {

    @Reference
    private OrderSettingService orderSettingService;

    @Reference
    private MemberService memberService;

    //提交用户预约信息
    @Override
    @LcnTransaction
    public Order add(Map map) {
        //获取手机号，验证码
        String  codeFromMap = (String)map.get("validateCode");
        String  telephone = (String)map.get("telephone");
        String redisCode  = RedisUtil.get(RedisMessageConstant.SENDTYPE_ORDER +telephone);
        //1、校验手机验证码是否过期
        if(StringUtils.isBlank(redisCode)){
             throw new RuntimeException("验证码已过期");
        }
        //2、判断用户是否输入验证码
        if(StringUtils.isBlank(codeFromMap)){
            throw new RuntimeException("请输入验证码");
        }
        //3、判断用户输入的验证码是否正确
        if(! redisCode.equals(codeFromMap)){
            throw new RuntimeException("验证码错误");
        }
        //4、判断当前时期是否还能预约
        //获取当前日期
        String  orderDate = (String) map.get("orderDate");
        int count = orderSettingService.isOrderOK(orderDate);
        if(count <= 0){
            throw new RuntimeException("当前日期预约人数已满，请选择其他日期");
        }
        //5、判断当前用户是否是新用户和重复预约 orderDate、member_id、SetMeal_id
        String sid = (String) map.get("setmealId");
        //查询会员表判断是否存在当前用户
        Member member = memberService.findMemberByTelephone(telephone);
        int mid = 0;
        if(member != null){
            //老用户——判断是否重复预约
            int orderExist = baseMapper.findOrderBySetMealIdAndOrderDateAndMemberId(Integer.parseInt(sid),orderDate,member.getId());
            mid =member.getId();
            if(orderExist > 0){
                throw  new RuntimeException("该日期您已预约，请勿重复预约");
            }
        }else{
            //新用户 - 插入新用户数据
         member = new Member();
         member.setName((String)map.get("name"));
         member.setSex((String)map.get("sex"));
         member.setIdCard((String)map.get("idCard"));
         member.setPhoneNumber(telephone);
         member.setRegTime(new Date(System.currentTimeMillis()));
         mid = memberService.saveMember(member);
        }
        //预约成功，更新预约表数据
        orderSettingService.updateReservationByOrderDate(orderDate);
        //更新订单表数据
        Order  order = new  Order();
        order.setMemberId(mid);
        order.setOrderDate(DateUtils.parseString2Date(orderDate,"yyyy-MM-dd"));
        order.setOrderType((String)map.get("orderType"));
        order.setSetmealId(Integer.parseInt(sid));
        order.setOrderStatus("未到诊");
        order.setStatus(0);
        order.setOrderId(SnowflakeIdWorker.getOrderId());
        save(order);
        // 订单创建成功加入Redis
        RedisUtil.addToSet(RedisConstant.SET_ORDER_ID, order.getOrderId());
        // 60秒过期
        RedisUtil.set(RedisConstant.ORDER_OID + order.getOrderId(), order.getOrderId(), 60, TimeUnit.SECONDS);
        return order;
    }

    @Override
    public Map findOrderInfoByOrderId(int id) {
        Map map = baseMapper.findOrderInfoByOrderId(id);
        String orderDate = String.valueOf(map.get("orderDate"));
        map.replace("orderDate",orderDate);
        return map;
    }


    /**
     * 订单存在并且为未支付的状态
     *
     * @param oid 订单编号
     * @return true：存在 false：不存在
     * @author liusen
     */
    @Override
    public boolean isExistOrderId(String oid) {
        return baseMapper.isExistOrderId(oid) == 1 ? true : false;
    }

    /**
     * 通过订单号查询手机号
     *
     * @param oid 订单号
     * @return 会员手机号
     * @author liusen
     */
    @Override
    public String findPhonenumberByOid(String oid) {
        Map<String, String> phonenumberByOid = baseMapper.findPhonenumberByOid(oid);
        return phonenumberByOid.get("phonenumber");
    }

    /**
     * 支付成功，通过订单号修改订单状态
     *
     * @param oid 订单号
     * @author liusen
     */
    @Override
    public void updateStatus(String oid) {
        baseMapper.updateStatus(oid);
    }

    /**
     * 查询订单状态信息
     *
     * @param oid 订单号
     * @author liusen
     */
    @Override
    public Integer findPayStatus(String oid) {
        Integer payStatus = baseMapper.findPayStatus(oid);
        System.out.println("查询订单状态：payStatus ======= " + payStatus);
        if (payStatus == null) {
            throw new RuntimeException("订单编号失效！");
        }
        return payStatus;
    }

    /**
     * 删除失效订单方法
     *
     * @param oid 订单id
     * @author liusen
     */
    @Override
    @LcnTransaction
    public void deleteOrderByOrderId(String oid) {
        // 删除订单
        String orderDate = baseMapper.findOrderDateByOid(oid);
        if (orderDate == null) {
            return;
        }
        baseMapper.deleteOrderByOrderId(oid);
        // 修改当日预约数
        orderSettingService.updateReservationByOrderDateDec(orderDate);
    }

    /**
     * xxl-job清理失效订单
     *
     * @author liusen
     */
    @Override
    public void clearOrder() {
        // 获取Redis中set集合中的所有元素
        Set<String> membersOfSet = RedisUtil.getMembersOfSet(RedisConstant.SET_ORDER_ID);
        // 判断集合是否为空
        if (membersOfSet.size() != 0) {
            // 遍历集合
            for (String orderId : membersOfSet) {
                // 判断Redis中是否存在相同值的key-value
                boolean existsKey = RedisUtil.existsKey(RedisConstant.ORDER_OID + orderId);
                System.out.println("existsKey = " + existsKey);
                if (existsKey == false) {
                    // 删除数据库失效订单
                    deleteOrderByOrderId(orderId);
                    // 删除set集合中的信息
                    RedisUtil.removeSetMember(RedisConstant.SET_ORDER_ID, orderId);
                    System.out.println("从数据库中删除了失效订单：" + orderId);
                }
            }
        }
    }

    /**
     * 吕重阳
     * */

    //查询运营报表数据
    @Override
    @LcnTransaction
    public Integer findTodayOrderNumber() {
        return baseMapper.findTodayOrderNumber();
    }

    //查询运营报表数据
    @Override
    @LcnTransaction
    public Integer findTodayVisitsNumber() {
        return baseMapper.findTodayVisitsNumber();
    }

    //查询运营报表数据
    @Override
    @LcnTransaction
    public Integer findThisWeekOrderNumber() {
        return baseMapper.findThisWeekOrderNumber();
    }

    //查询运营报表数据
    @Override
    @LcnTransaction
    public Integer findThisWeekVisitsNumber() {
        return baseMapper.findThisWeekVisitsNumber();
    }

    //查询运营报表数据
    @Override
    @LcnTransaction
    public Integer findThisMonthOrderNumber() {
        return baseMapper.findThisMonthOrderNumber();
    }

    //查询运营报表数据
    @Override
    @LcnTransaction
    public Integer findThisMonthVisitsNumber() {
        return baseMapper.findThisMonthVisitsNumber();
    }

    //查询运营报表数据
    @Override
    @LcnTransaction
    public List<Map<String,Object>> findHotSetmeal() {
        return baseMapper.findHotSetmeal();
    }

}
