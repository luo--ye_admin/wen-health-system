package com.wen.health;


import com.codingapi.txlcn.tc.config.EnableDistributedTransaction;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author 十一
 * @Description
 * @create 2020-11-12-21:58
 */
@SpringBootApplication
@MapperScan(basePackages = "com.itheima.health.mapper")
@EnableDistributedTransaction
public class OrderApplication {
    public static void main(String[] args) {
        SpringApplication.run(OrderApplication.class,args);
        System.out.println("---订单服务模块服务启动-----");
    }
}
