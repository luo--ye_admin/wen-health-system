package com.wen.health.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wen.health.pojo.Order;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;
import java.util.Map;

/**
 * @author 十一
 * @Description
 * @create 2020-11-12-19:54
 */
public interface OrderMapper extends BaseMapper<Order> {

    @Select("select count(1) from t_order where SETMEAL_ID = #{sid} and ORDERDATE= #{orderDate} and MEMBER_ID = #{id}")
    int findOrderBySetMealIdAndOrderDateAndMemberId(@Param("sid") int sid, @Param("orderDate") String orderDate, @Param("id") Integer id);

    @Select("select ts.name as setmeal , tm.name as member , ts.price , tor.ORDERTYPE as orderType , tor.ORDERDATE as orderDate, tor.ORDER_ID as orderId " +
            "from  t_setmeal ts,t_order tor ,t_member tm " +
            "where   tor.SETMEAL_ID = ts.id and tor.MEMBER_ID = tm.id  " +
            "and  tor.id = #{id}")
    Map findOrderInfoByOrderId(@Param("id") int id);

    //====================================================================

    @Select("select count(1) from t_order where ORDER_ID = #{oid} and STATUS = 0")
    int isExistOrderId(@Param("oid") String oid);

    @Select("select tm.PHONENUMBER AS phonenumber from t_order tor, t_member tm where tor.MEMBER_ID = tm.ID and tor.ORDER_ID = #{oid} ")
    Map<String, String> findPhonenumberByOid(@Param("oid") String oid);

    @Update("update t_order set STATUS = 1 where ORDER_ID = #{oid} ")
    void updateStatus(@Param("oid") String oid);

    @Select("select STATUS from t_order where ORDER_ID = #{oid} ")
    Integer findPayStatus(@Param("oid") String oid);

    @Delete("delete from t_order where ORDER_ID = #{orderId} ")
    void deleteOrderByOrderId(@Param("orderId") String orderId);

    @Select("select ORDERDATE from t_order where ORDER_ID=#{oid} ")
    String findOrderDateByOid(@Param("oid") String oid);

    //====================================================================

    @Select("SELECT count(1) FROM t_order WHERE ORDERDATE = CURDATE()")
    Integer findTodayOrderNumber();

    @Select("SELECT count(1) FROM t_order WHERE ORDERDATE = CURDATE() AND ORDERSTATUS = '已到诊'")
    Integer findTodayVisitsNumber();

    @Select("SELECT COUNT(1) FROM t_order WHERE WEEK(ORDERDATE,1) = WEEK(NOW(),1)")
    Integer findThisWeekOrderNumber();

    @Select("SELECT COUNT(1) FROM t_order WHERE WEEK(ORDERDATE,1) = WEEK(NOW(),1) AND ORDERSTATUS = '已到诊'")
    Integer findThisWeekVisitsNumber();

    @Select("SELECT COUNT(1) FROM t_order WHERE MONTH(ORDERDATE) = MONTH(NOW())")
    Integer findThisMonthOrderNumber();

    @Select("SELECT COUNT(1) FROM t_order WHERE MONTH(ORDERDATE) = MONTH(NOW()) AND ORDERSTATUS = '已到诊'")
    Integer findThisMonthVisitsNumber();

    @Select("SELECT ts.name, COUNT(1) as setmealCount, COUNT(1)/(SELECT COUNT(1) FROM t_order) as proportion, ts.remark " +
            "FROM t_order tor, t_setmeal ts " +
            "WHERE tor.SETMEAL_ID = ts.ID " +
            "GROUP BY tor.SETMEAL_ID ASC " +
            "LIMIT 0,3")
    List<Map<String, Object>> findHotSetmeal();

}
