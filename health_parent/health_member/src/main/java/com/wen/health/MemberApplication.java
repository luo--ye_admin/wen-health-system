package com.wen.health;


import com.codingapi.txlcn.tc.config.EnableDistributedTransaction;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author 十一
 * @Description
 * @create 2020-11-12-21:53
 */
@SpringBootApplication
@MapperScan(basePackages = "com.itheima.health.mapper")
@EnableDistributedTransaction
public class MemberApplication {
    public static void main(String[] args) {
        SpringApplication.run(MemberApplication.class,args);
        System.out.println("---会员模块服务启动-----");
    }
}
