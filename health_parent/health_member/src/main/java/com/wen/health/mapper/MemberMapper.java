package com.wen.health.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wen.health.pojo.Member;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * @author 十一
 * @Description
 * @create 2020-11-12-20:38
 */
public interface MemberMapper extends BaseMapper<Member> {

    @Select("select  id,PHONENUMBER  from t_member where PHONENUMBER = #{telephone}")
    Member findMemberByTelephone(@Param("telephone") String telephone);

    //=======================================
    //会员占比
    @Select(" SELECT (CASE  when m.age<=0 then '年龄非法' " +
            "WHEN m.age BETWEEN 1 AND 18 THEN '0-18岁' " +
            "              WHEN m.age BETWEEN 19 AND 30 THEN '18-30岁' " +
            "              WHEN m.age BETWEEN 31 AND 45 THEN '30-45岁' " +
            "              WHEN m.age > 45 THEN '45岁以上'  " +
            "              end ) as name ,COUNT(1) as  'value'  " +
            "              FROM  " +
            "              (SELECT NOW() AS '当前系统时间',birthday,TIMESTAMPDIFF(YEAR, birthday, CURDATE()) as age  from t_member ) as m  " +
            "              GROUP BY name")
    List<Map> findMemberProportionByAge();

    @Select(" SELECT (case " +
            "  when sex=0 then '女' else '男' end ) as name, " +
            " count(id) as value " +
            " from t_member  " +
            " GROUP BY sex")
    List<Map> findMemberProportionBySex();

    //============================
    @Select("SELECT count(1) FROM t_member WHERE REGTIME = CURDATE()")
    Integer findTodayNewMember();

    @Select("SELECT count(1) FROM t_member")
    Integer findTotalMember();

    @Select("SELECT COUNT(1) FROM t_member WHERE WEEK(REGTIME,1) = WEEK(NOW(),1)")
    Integer findThisWeekNewMember();

    @Select("SELECT COUNT(1) FROM t_member WHERE MONTH(REGTIME) = MONTH(NOW())")
    Integer findThisMonthNewMember();
}
