package com.wen.health.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.codingapi.txlcn.tc.annotation.LcnTransaction;
import com.wen.health.mapper.MemberMapper;
import com.wen.health.pojo.Member;
import com.wen.health.service.MemberService;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;


/**
 * @author 十一
 * @Description
 * @create 2020-11-06-18:53
 */
@Service
@Transactional
public class MemberServiceImpl extends ServiceImpl<MemberMapper, Member> implements MemberService {


    @Override
    public Member findMemberByTelephone(String telephone) {
        return baseMapper.findMemberByTelephone(telephone);
    }

    @Override
    @LcnTransaction
    public int saveMember(Member member) {
        save(member);
        return member.getId();
    }

    /**
     * 会员占比（按照年龄阶段进行划分）
     * @return
     */
    @Override
    public List<Map> findMemberProportionByAge() {
        return baseMapper.findMemberProportionByAge();
    }

    @Override
    public List<Map> findMemberProportionBySex() {
        return baseMapper.findMemberProportionBySex();
    }

    //=======================================
    //查询运营报表数据
    @Override
    @LcnTransaction
    public Integer findTodayNewMember() {
        return baseMapper.findTodayNewMember();
    }

    //查询运营报表数据
    @Override
    @LcnTransaction
    public Integer findTotalMember() {
        return baseMapper.findTotalMember();
    }

    //查询运营报表数据
    @Override
    @LcnTransaction
    public Integer findThisWeekNewMember() {
        return baseMapper.findThisWeekNewMember();
    }

    //查询运营报表数据
    @Override
    @LcnTransaction
    public Integer findThisMonthNewMember() {
        return baseMapper.findThisMonthNewMember();
    }

}
